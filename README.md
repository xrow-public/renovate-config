# Renovate config

Hint: The token for the registry.redhat.io registry is a harmless readonly token.

## Usage

Place a file called `renovate.json` in your project root.

```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "local>xrow-public/renovate-config"
  ]
}
```
## Run local

```json
npm i -g renovate
LOG_LEVEL=debug npx renovate --platform=local
```

Inspect the results